class MethodReturn {
  constructor () {
    this.type = 'void'
    this.description = null
  }
}

module.exports.MethodReturn = MethodReturn
