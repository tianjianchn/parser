class SlotProp {
  constructor (name = null, type = 'Any', description = '') {
    this.name = name
    this.type = type
    this.description = description
  }
}

module.exports.SlotProp = SlotProp
